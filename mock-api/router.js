var jsonServer = require('json-server');
var server = jsonServer.create();
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('db.json');
const db = low(adapter);

dominio = 'http://localhost:3004';

API_rest = {
    token: "/oauth/token"
};

// Agregar rutas personalizadas desplegadas con el ruteador del servidor JSON Server
server.get(API_rest.token, function (req, res) {
    // See https://github.com/typicode/lowdb
    var user = db.get("oauth");

    if (user) {
        res.jsonp(user);
    } else {
        res.sendStatus(404);
    }
})

server.use(function (req, res, next) {
    if (req.method === 'POST') {
        req.body.createdAt = Date.now();
    }
    // Continue to JSON Server router
    next();
})

// Usar la ruta por default
// server.use(router)
server.listen(3004, function () {
    console.log('JSON Server is running on:');
    console.log(dominio);
    console.log('\n');
    console.log('Routes:');
    console.log(dominio+API_rest.token);
})