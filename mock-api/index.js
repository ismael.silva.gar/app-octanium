/**
 * definir la estructura Json de los datos y algunos datos de prueba
 */
const { resolve, extend } = require('json-schema-faker');
const fs = require('fs');
extend('faker', () => require('faker'));

const schema = {
    type: "object",
    required: ["oauth"],
    properties: {
        oauth: {
            type: "array",
            maxItems: 1,
            items: { "$ref": "#/definitions/oauth" }
        }
    },
    definitions: {
        oauth: {
            type: "object",
            required: ["id", "expires_in"],
            properties: {
                id: {
                    $ref: '#/definitions/positiveInt'
                },
                expires_in: {
                    $ref: '#/definitions/positiveIntRandom'
                }
            }
        },
        positiveInt: {
            type: 'integer',
            minimum: 0,
            exclusiveMinimum: true
        },
        positiveIntRandom: {
            type: 'integer',
            maximum: 100,
            minimum: 0,
            exclusiveMinimum: true
        }
    }
}
resolve(schema).then(sample => {
    console.log('Writing to db.json')
    fs.writeFile(`${__dirname}/db.json`, JSON.stringify(sample), function (err) {
        if (err) {
            console.error(err);
        } else {
            console.log("done data created succesfully");
        }
    });
});