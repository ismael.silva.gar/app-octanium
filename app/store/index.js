import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import axios from 'axios';
import parameters from '../parameters';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  state:{
    tokens:{
      access_token: null,
      expires_in: null,
      refresh_token: null,
      token_type: null
    },
    currentUser: {
      id: 0,
      name: null,
      email: null,
      role: null,
    },
    items:[],
    isActiveMenuButton:[],
    isSelectedPage:[],
    isEnabledMenuButton:[],
    isEnabledListLabelButton:[],
    isEnabledOptionButton:[]
  },
  getters:{
    getItems(state){ return state.items},
    getIsActiveMenuButton: state => id =>{
      // store.commit('get_index/getIndex', {id: id, items: state.items});
      // var i = store.state.get_index.index; 
      var i = state.items.map(function(e) { return e.id; }).indexOf(id);
      return typeof state.isActiveMenuButton[i] !=="undefined"?state.isActiveMenuButton[i].isActive:false;
    },
    getIsEnabledMenuButton: state => id =>{
      var i = state.items.map(function(e) { return e.id; }).indexOf(id);
      return typeof state.isEnabledMenuButton[i] !=="undefined"?state.isEnabledMenuButton[i].isEnabled:true;
    },
    getIsEnabledListLabelButton: state => id =>{
      var i = state.items.map(function(e) { return e.id; }).indexOf(id);      
      return typeof state.isEnabledListLabelButton[i] !=="undefined"?state.isEnabledListLabelButton[i].isEnabled:true;
    },
    getIsEnabledOptionButton: state => id_tipo =>{
      var i = state.items.map(function(e) { return e.id; }).indexOf(id_tipo.id);        
      if(id_tipo.tipo == 4) // Tipo 4
        return typeof state.isEnabledOptionButton[i] !=="undefined"?state.isEnabledOptionButton[i][3].isEnabled:false;
      else if(id_tipo.tipo == 3) // tipo 3
        return typeof state.isEnabledOptionButton[i] !=="undefined"?state.isEnabledOptionButton[i][2].isEnabled:false;
      else if(id_tipo.tipo == 2) // tipo 2
        return typeof state.isEnabledOptionButton[i] !=="undefined"?state.isEnabledOptionButton[i][1].isEnabled:false;
      else if(id_tipo.tipo == 1) // tipo 1
        return typeof state.isEnabledOptionButton[i] !=="undefined"?state.isEnabledOptionButton[i][0].isEnabled:false;
      else // tipo 5
        return typeof state.isEnabledOptionButton[i] !=="undefined"?state.isEnabledOptionButton[i][4].isEnabled:false;
    },
    getSelectedPage: state => index => {
      if(state.isSelectedPage.length !== 0){
        return state.isSelectedPage[index].isActive;
      }
    },
    getUser: state => state.currentUser
  },
  actions:{
    setItems(context, items){
      context.commit('setItems', items);
    },
    setIsActiveMenuButton(context, id){
      context.commit('setIsActiveMenuButton', id);
    },
    setIsEnabledOptionButton(context, id){
      context.commit('setIsEnabledOptionButton', id);
    },
    setSelectedPage(context, noItems_i){
      context.commit('setSelectedPage', noItems_i);
    },
    login(context, user){
      // return new Promise(function(resolve, reject){
      let data = {
        client_id: parameters.client_id,
        client_secret: parameters.client_secret,
        grant_type: 'password',
        username: user.email,
        password: user.password,
      };
      return axios.post(parameters.urlApi+'/oauth/token',data).then(response => {
      let reponseData = response.data;
          let now = Date.now();              
          reponseData.expires_in = reponseData.expires_in + now;
          context.commit('updateTokens',reponseData);
          console.log('vuex');
          
          return response;
          // resolve(response);
        // }).catch(response =>{reject(response)});
      });
    },
    resetPassword(context, email){
      return axios.post(parameters.urlApi+'/api/v1/password/email', {"email":email}).then(response => {
        if (response.data === '') {
          return {data:{message:"error de conexion al servidor"}};
        }  
        return response;
      });
    },
    setUser(context, user){
      return new Promise((resolve, reject) => {
        let userData = {
          id: 0,
          name: null,
          email: null,
          role: null,
        };
        
        axios.get(parameters.urlApi+`/api/v1/mtto/user/${user.email}/datos`)
        .then(respuesta => {
          let resp = respuesta.data[0];
          userData.id = resp.id;
          userData.name = resp.name;
          userData.email = resp.email;
          userData.role = typeof resp.roles.find((element)=>{ return element.name==="supervisor"; })!=="undefined"?4:3;  
          context.commit('setUser', userData);
          resolve(respuesta)
          
        })
        .catch(response =>{reject(response)});
      });
    },
    logOut(context){
      context.commit('setLogOut');
    },
  },
  mutations:{
    setItems(state, items){
      for (let i = 0; i < 2; i++) {
        if (i == 0) {
          state.isSelectedPage.push({isActive:true});  
        } else {
          state.isSelectedPage.push({isActive:false});
        }
      }
      for (let i = 0; i < items.length; i++) {
        state.isActiveMenuButton.push({isActive:false});
        state.isEnabledMenuButton.push({isEnabled:true});
        state.isEnabledListLabelButton.push({isEnabled:true});
        state.isEnabledOptionButton.push([{isEnabled:false}, {isEnabled:false}, {isEnabled:false}, {isEnabled:false}, {isEnabled:false}]);
      }
      state.items = items;
      // console.log('item: ', state.isActiveMenuButton, ' long: ', state.isActiveMenuButton.length);
    },
    setIsActiveMenuButton(state, id){
      var i = state.items.map(function(e) { return e.id; }).indexOf(id);      
      state.isActiveMenuButton[i].isActive = !state.isActiveMenuButton[i].isActive;
      for(let j = 0; j < state.isActiveMenuButton.length; j++) {
        if(j != i)
          state.isEnabledMenuButton[j].isEnabled = !state.isEnabledMenuButton[j].isEnabled;

        state.isEnabledListLabelButton[j].isEnabled = !state.isEnabledListLabelButton[j].isEnabled;
      }
    },
    setIsEnabledOptionButton(state, id_status){
      var i = state.items.map(function(e) { return e.id; }).indexOf(id_status.id);      
      state.isEnabledOptionButton[i].forEach(element => {
        element.isEnabled = false;
      });
      
      if(id_status.status == 1){ // Tipo 4
        state.isEnabledOptionButton[i][3].isEnabled = true;
      }else if(id_status.status == 2){ // tipo 3
        state.isEnabledOptionButton[i][2].isEnabled = true;
      }else if(id_status.status == 3){ // tipo 2
        state.isEnabledOptionButton[i][1].isEnabled = true;
        state.isEnabledOptionButton[i][0].isEnabled = true;
      }
      // tipo 5
      state.isEnabledOptionButton[i][4].isEnabled = true;      
    },
    setSelectedPage(state, noItems_i){   
      for (let i = 0; i < 2; i++) {
          if(noItems_i.index == i)
            state.isSelectedPage[i].isActive = true;
          else
            state.isSelectedPage[i].isActive = false;
      }      
    },
    updateTokens(state, tokens){
      state.tokens = tokens;
    },
    setUser(state, user){
      state.currentUser = user;
    },
    setLogOut(state){
      state.currentUser = {
        name: null,
        email: null,
        role: null,
      };
      
      state.isActiveMenuButton=[];
      state.isEnabledMenuButton=[];
      state.isEnabledListLabelButton=[];
      state.isEnabledOptionButton=[];
      state.isSelectedPage=[];
    },
  }
});

Vue.prototype.$store = store;

export default store