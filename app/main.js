import Vue from 'nativescript-vue'
import Login from './pages/Login'
import VueDevtools from 'nativescript-vue-devtools'
import store from "./store"
import parameters from "./parameters"
import './filters'
// import UI nativescript components
import RadListView from 'nativescript-ui-listview/vue';
Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer);
Vue.registerElement('CheckBox',	() => require('nativescript-checkbox').CheckBox,
	{
		model: {
			prop: 'checked',
			event: 'checkedChange'
		}
	}
);
// constants
export const EventBus = new Vue();
export const urlApi = parameters.urlApi;

Vue.use(RadListView);

// if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
// }
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

new Vue({
  store,
  render: h => h('frame', [h(Login)])
}).$start()
